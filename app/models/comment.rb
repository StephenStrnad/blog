class Comment < ActiveRecord::Base
	has_one :Post
	validates_presence_of :post_id
	validates_presence_of :body
end
